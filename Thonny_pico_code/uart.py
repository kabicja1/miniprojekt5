import time
import random


# Hlavní smyčka programu
while True:
    # Generování náhodné teploty (simulace)
    temperature = random.uniform(18.0, 28.0)  # Náhodná teplota mezi 18.0 a 28.0 stupni Celsia
    
    # Převod teploty na řetězec
    temperature_str = "{:.2f}".format(temperature)
    
    # Odeslání teploty přes sériové rozhraní
    print(temperature_str)
    
    # Přestávka po dobu 20 sekund
    time.sleep(20)
