import network
import time
from umqtt.simple import MQTTClient
import random

# Konfigurace Wi-Fi sítě
SSID = "Kabickovi_Wifi"
PASSWORD = "doma12wifi"

# Konfigurace MQTT
mqtt_server = "192.168.1.111"
client = MQTTClient("pico_client", mqtt_server, 1883)

def connect_wifi():
    wlan = network.WLAN(network.STA_IF)  
    wlan.active(True)  

    if not wlan.isconnected():
        print(f"Připojuji se k síti {SSID}...")
        wlan.connect(SSID, PASSWORD)

        
        timeout = 10  
        start_time = time.time()
        while not wlan.isconnected():
            if (time.time() - start_time) > timeout:
                print("Připojení k síti se nezdařilo. Zkontrolujte připojení.")
                break
            time.sleep(1)

    if wlan.isconnected():
        print(f"Připojení k síti {SSID} úspěšné.")
    else:
        print(f"Nepodařilo se připojit k síti {SSID}.")
        return False

    return True

def send_data_over_mqtt():
    while True:
        if connect_wifi():
            temperature = random.uniform(18.0, 28.0)
            temperature_str = "{:.2f}".format(temperature)
            print(temperature_str)
            # Připojení k MQTT brokeru
            if client.connect() == 0:
                print("Připojení k MQTT brokeru úspěšné.")
                client.publish("/esp32/temperature", temperature_str)
            else:
                print("Nepodařilo se připojit k MQTT brokeru. Chyba:", client.last_error())

        
        time.sleep(20)

send_data_over_mqtt()
