from flask import Blueprint, render_template, url_for, redirect, flash
from flask_login import login_required, current_user, login_user, logout_user
from flask_bcrypt import Bcrypt
from sqlalchemy import func

web_routes_bp = Blueprint('web_routes', __name__, static_folder='static', static_url_path='/static')

bcrypt = Bcrypt()

@web_routes_bp.route('/', methods=['GET', 'POST'])
@login_required
def dashboard(): 
    
    from app import User, Measurement, db
    measurements = Measurement.query.all() 
    max_timestamp = db.session.query(Measurement.timestamp).filter(Measurement.id == db.session.query(db.func.max(Measurement.id))).scalar()
    max_temp = db.session.query(Measurement.temp).filter(Measurement.id == db.session.query(db.func.max(Measurement.id))).scalar()
    return render_template('dashboard.html', name=current_user.username, measured_values=measurements, max_timestamp=max_timestamp, max_temp = max_temp) 

@web_routes_bp.route('/login', methods=['GET', 'POST'])
def login(): 
    from app import LoginForm, User
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('web_routes.dashboard'))
    return render_template('login.html', form=form)  

@web_routes_bp.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('web_routes.login'))

@web_routes_bp.route('/register', methods=['GET', 'POST'])
def register():  
    from app import RegisterForm, User, db
    form = RegisterForm()
    if form.validate_on_submit():

        username = form.username.data

        # Kontrola, zda uživatelské jméno již existuje
        existing_user = User.query.filter_by(username=username).first()
        if existing_user:
            flash('That username is already taken. Please choose a different one.', 'danger')
            return render_template('register.html', form=form)
    
        hashed_password = bcrypt.generate_password_hash(form.password.data)
        new_user = User(username=form.username.data, password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        
        # Automatické přihlášení nově registrovaného uživatele
        login_user(new_user)
        return redirect(url_for('web_routes.dashboard'))  # Přesměrování na dashboard po registraci

    return render_template('register.html', form=form)  
